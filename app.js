function Canvas() {
	this.canvas = null;
	this.ctx = null;
	this.imagen = new Image();
}

Canvas.prototype = {
	inicio: function() {
		if (this.canvas && this.canvas.getContext) {
			this.ctx = this.canvas.getContext("2d");
			if (this.ctx) {
				return this.ctx
			} else {
				return null;
			} //fin else

		} //fin del if
	}, //fin incio
	procesaImagen: function() {
		canvas.limpiar();
		this.ctx.drawImage(this.imagen, 10, 10);
	},
	filtro: function() {},//fin filtro
	limpiar: function() {},//fin limpiar
	cargar: function(archivo) {
		this.imagen.src = "./img" + archivo;
		this.imagen.onload = function(e) {
			canvas.procesaImagen
		}; //fin onload
	}, //fin cargar
	dibujar: function() {}//fin dibujar
}; //fin canvas.prototype

function Filtro(canvas) {

	this.w = canvas.imagen.width;
	this.h = canvas.imagen.height;
	this.imagenData = canvas.ctx.getImageData(10, 10, this.w, this.h)
	this.data = imagenData.data;
}
window.onload = function() {

	//
	var archivoSelect = document.getElementById("archivo");
	var filtroSelect = document.getElementById("filtro");
	//Eventos

	archivoSelect.onchange = function(e) {
		canvas.cargar(archivoSelect.value);
	};
	filtroSelect.onchange = function(e) {
		canvas.filtro(filtroSelect.value);
	};
	//codigo
	canvas = new Canvas();
	canvas.canvas = document.getElementById("canvas");
	if (canvas.inicio) {
		canvas.cargar("perro1.jpg");
	} else {
		alert("Tu navegador no soporta HTML5");
	};
};